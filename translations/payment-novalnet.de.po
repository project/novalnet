msgid ""
msgstr ""
"Project-Id-Version: Novalnet Translations (7.x-1.0-beta2)\n"
"POT-Creation-Date: 2011-07-14 23:22+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: German\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"


# German translation of ubercart2.4 Novalnet Payment modules
# Copyright (c) 2011 by Novalnet AG
# Generated from files:
# uc_novalnet_cc_pci.module,v 2.4 2011/04/21 Novalnet AG
#
msgid ""
msgstr ""
"Project-Id-Version: commerce (6.x-2.0-beta1)\n"
"POT-Creation-Date: 2011-04-21 \n"
"PO-Revision-Date: 2011-04-21 \n"
"Language-Team: German\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"


msgid "Novalnet Credit Card"
msgstr "Novalnet Kreditkarte"

msgid "Credit Card"
msgstr "KreditKarte"

#: /
msgid "Novalnet Credit Card Pci"
msgstr "Novalnet Krdeitkarte Pci"

msgid "Novalnet Direct Debit Austria"
msgstr "Novalnet Lastschrift Österreich"

msgid "Direct Debit Austria"
msgstr "Lastschrift Osterreich"

#: /
msgid "Novalnet Direct Debit Austria Pci"
msgstr "Novalnet Lastschrift Osterreich Pci"

msgid "Direct Debit Austria Pci"
msgstr "Lastschrift Osterreich Pci"

msgid "Novalnet Direct Debit German"
msgstr "Novalnet Lastschrift Deutschland"

msgid "Direct Debit German"
msgstr "Lastschrift-Deutschland"

#: /
msgid "Novalnet Direct Debit German Pci"
msgstr "Novalnet Lastschrift Deutschland Pci "

msgid "Direct Debit German Pci"
msgstr "Lastschrift Deutschland Pci"

msgid "Novalnet Invoice"
msgstr "Novalnet Rechnung"

msgid "Invoice"
msgstr "Rechnung"

msgid "Novalnet Prepayment"
msgstr "Novalnet Vorkasse"

msgid "Prepayment"
msgstr "Vorkasse"

msgid "Card Holder Name"
msgstr "Name des Karteninhabers"

msgid "Credit Card No"
msgstr "Kreditkarte Nummer"

msgid "Expiry Month"
msgstr "Monat des Ablaufdatums"

msgid "Expiry Year"
msgstr "Jahr des Ablaufdatums"

msgid "CVV/CVC"
msgstr "CVV / CVC"

#: /?q=admin/modules
msgid "Payment Using Novalnet Direct Debit Austria Pci"
msgstr "Novalnet Lastschrift Osterreich"

#: /?q=admin/modules
msgid "Payment Using Novalnet Direct Debit German Pci"
msgstr "Novalnet Lastschrift Deutschland Pci "

msgid "Bank Account Owner"
msgstr "Bank Kontoinhaber"

msgid "Bank Account Number"
msgstr "Bank Kontonummer"

msgid "Bank Code"
msgstr "Bankleitzahl"

msgid "Enable ACDC Check"
msgstr "ACDC Check aktivieren"

msgid "Please transfer the amount to following account:"
msgstr "Bitte ueberweisen Sie den Rechnungsbetrag auf folgendes Konto:"

#: /?q=checkout/109/review
msgid "Please transfer the amount at the latest, untill"
msgstr "Bitte überweisen Sie den Betrag spätestens bis zum"

msgid "Account holder:NOVALNET AG"
msgstr "Kontoinhaber: Novalnet AG"

msgid "Account Number:"
msgstr "Kontonummer:"

msgid "BLZ:"
msgstr "BLZ:"

msgid "Bank:"
msgstr "Bank:"

msgid "IBAN:"
msgstr "IBAN:"

msgid "SWIFT / BIC:"
msgstr "SWIFT / BIC:"

msgid "Amount:"
msgstr "Betrag:"

msgid "Reference:"
msgstr "Verwendungszweck:"

msgid "Novalnet Transaction Id"
msgstr "Novalnet Transaktion Id"

msgid ""
"Please note that the Transfer can only be identified with the above "
"mentioned Reference."
msgstr "Bitte beachten Sie, dass die ueberweisung nur bearbeitet werden kann"

msgid "Novalnet Credit Card PCI"
msgstr "Novalnet Kreditkarten PCI Bezahlung"

#: payment/uc_novalnet/uc_novalnet_cc_pci.module:20
msgid "Credit Card PCI"
msgstr "Kreditkarten PCI"

msgid "You will be redirected to Novalnet AG Payment Gateway"
msgstr ""
"Sie werden auf das Novalnet Payment Gateway umgeleitet, wenn Sie eine "
"Bestellung abschicken."

msgid "Redirect the Customers to Paypal Website to Make Payment"
msgstr "Leiten Sie die Kunden zu PayPal-Website, um Zahlung zu leisten."

msgid "Redirect into Novalnet Payment Gateway."
msgstr "Umleitung zum Novalnet Payment Gateway."

msgid "Test Mode"
msgstr "Testmodus"

msgid "Do you want to activate test mode?"
msgstr "Wollen Sie den Test-Modus aktivieren ?"

msgid "There was an error and your payment could not be completed."
msgstr ""
"Ein Fehler trat auf und Ihre Zahlung konnte nicht abgeschlossen "
"werden."

msgid "Novalnet Transaction OK."
msgstr "Novalnet Transaktion OK."

msgid "TID:"
msgstr "TID:"

msgid "TESTORDER"
msgstr "TESTBESTELLUNG"

msgid "TEST ORDER"
msgstr "TESTBESTELLUNG"

msgid "<B>TEST ORDER</B>"
msgstr "<B>TESTBESTELLUNG</B>"

msgid "Novalnet Direct Debit Austria PCI"
msgstr "Novalnet Lastschrift &Ouml;sterreich PCI"

msgid "Direct Debit Austria PCI"
msgstr "Lastschrift &Ouml;sterreich PCI"

msgid "Novalnet Direct Debit German PCI"
msgstr "Novalnet Lastschrift Deutschland PCI"

msgid "Direct Debit German PCI"
msgstr "Lastschrift Deutschland PCI"

msgid "Novalnet Gateway"
msgstr "Novalnet Gateway"

msgid "Process credit card payments through the Test Gateway."
msgstr "Verarbeitung der Kreditkartenzahlungen durch das Test-Gateway."

msgid "Credit card charged: !amount"
msgstr "Kreditkarte belastet: !amount"

msgid "Credit card charge failed."
msgstr "Die Belastung der Kreditkarte schlug fehl."

msgid "Card charged, resolution code: 0022548315"
msgstr "Karte belastet, resolution code: 0022548315"

msgid "Credit card payment processed successfully."
msgstr "Die Kreditkarten-Zahlung wurde erfolgreich abgeschlossen."

msgid "Novalnet Credit Card 3D Secure"
msgstr "Novalnet Kreditkarte 3D Secure"

msgid "credit Card"
msgstr "Kreditkarte"

msgid "Call Back Method"
msgstr "Call Back Method"

msgid "Payment of "
msgstr "Zahlung von "

msgid "  submitted through  "
msgstr " eingereicht durch "

msgid "Pay via Credit card."
msgstr "Pay per Kreditkarte."

msgid "Credit Card 3D Secure"
msgstr "Kreditkarte 3D Secure"

msgid "Pay via Novalnet Gateway."
msgstr "Zahlung per Novalnet Gateway."

msgid "Please select your Expiration Month"
msgstr "Bitte waehlen Sie den Monat Ihres Ablaufdatums"

msgid "Please select your Expiration Year"
msgstr "Bitte waehlen Sie das Jahr Ihres Ablaufdatums"

msgid "Please enter your CVV / CVC Code"
msgstr "Bitte geben Sie Ihren CVV / CVC Code ein"

msgid "Second Product Id"
msgstr "Second Produkt-ID"

msgid "Enter the Novalnet Second Product Id"
msgstr "Geben Sie die Novalnet Zweites Produkt Id"

msgid "Second Tariff Id"
msgstr "Second Tarif Id"

msgid "Credit Card Number"
msgstr "Nummer der Kreditkarte"

msgid "Expiry Date"
msgstr "Verfallsdatum"

msgid "Enter the Novalnet Second Tariff Id"
msgstr "Geben Sie die Novalnet zweite Tarif Id"

msgid "Oops! Please Enter the telephone number."
msgstr "Oops! Bitte geben Sie die Telefonnummer."

msgid "Please Enter the Pin Number You Have received."
msgstr "Bitte geben Sie die Pin-Nummer You Have erhalten."

msgid "Card type"
msgstr "Kreditkarten-Typ"

msgid "Card owner"
msgstr "Kreditkarten-Inhaber"

msgid "Card number"
msgstr "Kreditkarten-Nummer"

msgid "Issuing bank"
msgstr "Ausstellendes Kreditinstitut"

msgid "Process card"
msgstr "Kreditkarte verarbeiten"

msgid "(Last 4) "
msgstr "(Letzte 4)"

msgid "Start Month"
msgstr "Anfangsmonat"

msgid "Start Year"
msgstr "Anfangsjahr"

msgid "Expiration Month"
msgstr "Monat des Ablaufdatums"

msgid "Expiration Year"
msgstr "Jahr des Ablaufdatums"

msgid "Issue Number"
msgstr "Ausgabenummer"

msgid "CVV"
msgstr "CVV"

msgid "Oops! Enter the Correct Pin number."
msgstr "Oops! Geben Sie die korrekte PIN-Nummer."

msgid "Issuing Bank:"
msgstr "Ausstellende Bank"

msgid "Pay via bank debit."
msgstr "Bezahlung per Bankeinzug."

msgid "Invalid Account Holder name"
msgstr "Bitte geben Sie einen Kontoinhaber ein"

msgid "Please Enter your Account Number"
msgstr "Bitte geben Sie eine Kontonummer ein"

msgid "Please Enter your Bank Code"
msgstr "Bitte geben Sie eine Bankleitzahl ein"

msgid "Account holder"
msgstr "Kontoinhaber"

msgid "Account No"
msgstr "Kontonr."

msgid "Bank code"
msgstr "Bankleitzahl"

msgid "Account Number"
msgstr "Kontonummer"

msgid "ACDC Control"
msgstr "ACDC Check"

msgid "Do you want to enable ACDC?"
msgstr "Wollen Sie ACDC aktivieren?"

msgid "Enter the Pin Number"
msgstr "Geben Sie die PIN-Nummer"

msgid "Enable ACDC Check !"
msgstr "Bitte akzeptieren Sie den ACDC Check"

msgid "Payment Duration"
msgstr "Zahlungsfrist in Tagen:"

msgid "Specify Payment Duration for Invoice Process"
msgstr "Zahlungsfrist fuer Rechnungszahlung"

msgid ""
"Account Holder : NOVALNET AG \n"
" "
msgstr "Kontoinhaber: Novalnet AG  n"

msgid "Account Number :"
msgstr "Konto-Nummer:"

msgid " to following account:"
msgstr " auf folgendes Konto:"

msgid "Account number:"
msgstr "Kontonummer:"

msgid "Bankcode:"
msgstr "Bankleitzahl:"

msgid "Swift:"
msgstr "Swift:"

msgid "Enter the New pin You have received !"
msgstr "Geben Sie die neue PIN, die Sie erhalten haben,!"

msgid ""
"Redirect customers to their online bank account to let them make a "
"transfer."
msgstr ""
"Die Kunden werden zu ihrem Online-Bankkonto umgeleitet, um eine "
"Ueberweisung durchzufuehren."

msgid "Encryption key"
msgstr "Chiffrierschluessel"

msgid "PAYPAL API UserName"
msgstr "PAYPAL API UserName"

msgid "Enter You PAYPAL API UserName"
msgstr "Geben Sie PAYPAL API UserName"

msgid ""
"The default value is only valid in test mode. For live use, get your "
"key from your account at !url -> Stammdaten -> "
"Paymentzugriffsschluessel."
msgstr ""
"Fuer Online-Zahlungen (Sofortueberweisung) muessen Sie ein Passwort im "
"Adminbereich eingeben. Sie koennen Ihr Passwort ueber das Admin-Tool "
"erhalten: https://admin.novalnet.de -> Stammdaten -> "
"Paymentzugriffsschluessel"

msgid "Payment of @amount @currency submitted through Sofortueberweisung.de."
msgstr ""
"Die Zahlung von @amount @currency durch Sofortueberweisung.de "
"vorgelegt."

msgid "You will be Redirected to Novalnet AG Payment Gateway."
msgstr "Sie werden auf Novalnet AG Payment Gateway Weitergeleitet."

msgid ""
"Payment of @amount @currency submitted through Novalnet Credit Card "
"PCI."
msgstr ""
"Die Zahlung der @amount @currency durch Novalnet Kreditkarte PCI "
"eingereicht."

msgid ""
"Payment of @amount @currency submitted through Novalnet Direct Debit "
"Austria PCI."
msgstr ""
"Die Zahlung der @amount @currency durch Novalnet Lastschrift "
"Österreich PCI eingereicht."

msgid ""
"Payment of @amount @currency submitted through Novalnet Direct Debit "
"German PCI."
msgstr ""
"Die Zahlung der @amount @currency durch Novalnet Lastschrift deutschen "
"PCI eingereicht."

msgid "Visa"
msgstr "Visa"

msgid "Mastercard"
msgstr "Mastercard"

msgid "Discover"
msgstr "Discover"

msgid "American Express"
msgstr "American Express"

msgid ""
"The path for the Novalnet handler script to return payment status is "
"automatically set to "
msgstr ""
"Der Pfad fuer die Novalnet Haendler-Skript zur Rueckgabe des "
"Zahlungsstatus steht automatisch auf "

msgid "Novalnet settings"
msgstr "Novalnet Einstellungen"

msgid "Credit Card Holder"
msgstr "Kreditkarten-Inhaber"

msgid "Exp Month"
msgstr "Exp Monat"

msgid "January"
msgstr "Januar"

msgid "Febraury"
msgstr "Febraur"

msgid "April"
msgstr "April"

msgid "May"
msgstr "Mai"

msgid "June"
msgstr "Juni"

msgid "July"
msgstr "Juli"

msgid "August"
msgstr "August"

msgid "September"
msgstr "September"

msgid "October"
msgstr "Oktober"

msgid "November"
msgstr "November"

msgid "December"
msgstr "Dezember"

msgid "Exp Year"
msgstr "Exp Jahr"

msgid "Handy/Mobile:"
msgstr "Handy/Mobil:"

msgid "Mobile:"
msgstr "Mobil:"

msgid "Novalnet Paypal"
msgstr "Novalnet Paypal"

msgid "Novalnet-Paypal"
msgstr "Novalnet-Paypal"

msgid "Redirect the Customers to Paypal Website to Make Payment."
msgstr "Leiten Sie die Kunden zu PayPal-Website, um Zahlung zu leisten."

msgid "Oops ! unusuall Payment Completion Order Cannot be completed."
msgstr ""
"Oops! unusuall Payment Fertigstellung Auftrag kann nicht abgeschlossen "
"werden."

msgid ""
"Novalnet only supports payments in Euros! Change your store "
"settings!here_link."
msgstr ""
"Novalnet unterstuetzt nur Zahlungen in Euro! Aendern Sie Ihre "
"Shop-Einstellungen !here_link."

msgid "Novalnet authority code"
msgstr "Novalnet Authentifizierungs-Code"

msgid "Novalnet vendor id"
msgstr "Novalnet Haendler-ID"

msgid "Novalnet tariff id"
msgstr "Novalnet Tarif-ID"

msgid "Novalnet product id"
msgstr "Novalnet Produkt-ID"

msgid "Novalnet CURL Timeout"
msgstr "Novalnet CURL Timeout"

msgid ""
"TEST MODE (Do you want to test Novalnet Credit Card module with TEST "
"MODE?)"
msgstr ""
"TEST MODUS (Wollen Sie das Novalnet Kreditkarten-Modul im TESTMODUS "
"testen?)"

msgid "Received by Novalnet AG"
msgstr "von Novalnet AG erhalten"

msgid "PAYPAL API Password:"
msgstr "PAYPAL API Passwort:"

msgid ""
"You will be redirected automatically. If not more than 5 seconds, "
"click here"
msgstr ""
"Sie werden automatisch weitergeleitet. Falls nach 5 Sekunden nicht, "
"dann bitte hier klicken"

msgid "Enter You PAYPAL API Password"
msgstr "Geben Sie PAYPAL API Passwort"

msgid "PAYPAL API Signature:"
msgstr "PAYPAL API Signatur:"

msgid "Enter You PAYPAL API Signature"
msgstr "Geben Sie PAYPAL API Signature"

msgid "Payment pending"
msgstr "Zahlung wird ausgeführt"

msgid "Novalnet Vendor ID:"
msgstr "Novalnet Haendler-ID"

msgid "Novalnet Authorisation Code:"
msgstr "Novalnet Authentifizierungs-Code"

msgid "Novalnet Product ID:"
msgstr "Novalnet Produkt-ID"

msgid "Novalnet Tariff ID:"
msgstr "Novalnet Tarif-ID"

msgid "Novalnet Manuall Booking Limit:"
msgstr "Novalnet Manuall Buchung Limit"

msgid "Novalnet Second Product ID:"
msgstr "Novalnet Second Produkt-ID"

msgid "Novalnet Second Tariff ID:"
msgstr "Novalnet Second Tarif-ID"

msgid "End User Information:"
msgstr "Ende Benutzer Informationen"

msgid "Input Data Missing"
msgstr "Eingang Daten vermisst"

#: /?q=checkout/111/review
msgid "Please enter the valid credit card details"
msgstr "Bitte geben Sie die gültige Kreditkarte"

msgid "Credit Card Holder name should be two or more characters long."
msgstr "Kreditkartenetui Name sollte zwei oder mehr Zeichen lang sein."

msgid "Bank account owner name should be two or more characters long."
msgstr "Bankverbindung Inhaber Name sollte zwei oder mehr Zeichen lang sein."

msgid "credit card Pci"
msgstr "Kreditkarten Pci"

#: /?q=checkout/111/review
msgid ""
"You will be redirected automatically. If not for more than 10 sec "
"click here"
msgstr ""
"Sie werden automatisch umgeleitet werden. Wenn nicht mehr als 10 sec "
"klicken Sie hier"

msgid "Credit Card Pci"
msgstr "Kreditkarte-Pci"

#: /?q=system/ajax
msgid "Novalnet Instant Bank Transfer"
msgstr "Novalnet Sofortueberweisung"

msgid "Instant Bank Transfer"
msgstr "Sofortueberweisung"

msgid ""
"You will be redirected automatically. If not more than 10 seconds, "
"click here"
msgstr ""
"Sie werden automatisch weitergeleitet. Falls nach 5 Sekunden nicht, "
"dann bitte hier klicken"

msgid "Redirecting...."
msgstr "Umleiten ...."

msgid "Novalnet Instant Bank Transfer "
msgstr "Novalnet Sofortueberweisung"

msgid "Please transfer the amount at the latest, untill "
msgstr "Bitte ueberweisen Sie den Betrag spaetestens bis zum "

msgid "Enter Your Novalnet Merchant ID"
msgstr "Geben Sie Novalnet Merchant ID"

msgid "Enter Your Novalnet  Authorisation Code"
msgstr "Geben Sie Novalnet Authorisation Code"

msgid "Enter Your Novalnet Product Id"
msgstr "Geben Sie Ihre Novalnet Produkt Id"

msgid "Enter Your Noalnet Tariff Id"
msgstr "Geben Sie Noalnet Tarif Id"

msgid "Enter Your Maximum Booking Amount"
msgstr "Geben Sie Maximum Buchungsbetrag"

msgid "Enter Your Second Product Id"
msgstr "Geben Sie Zweites Produkt Id"

msgid "Enter Your Second Tariff Id"
msgstr "Geben Sie Second Tarif Id"

msgid "Enter The Proxy Server Detail If Any"
msgstr "Enter The Proxy Server Detailansicht Wenn Jegliche"

msgid "Enter The Transaction Mode"
msgstr "Geben Sie die Transaktion Modus"

msgid "Enter The Encryption Password"
msgstr "Geben Sie die Encryption Password"

msgid "Do your want to enable ACDC?"
msgstr "Wollen Sie ACDC aktivieren?"
 
msgid "Please note down the below Transaction details provided by Novalnet (TID) <br>"
msgstr "Bitte notieren Sie die untenstehenden Transaktionsdetails von Novalnet (TID) <br>"
